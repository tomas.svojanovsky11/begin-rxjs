import { from, Observable, of } from "rxjs";

describe("Basic observables", () => {
  test("creates an observable from its arguments", () => {
    const result = [];

    const observable = of(1, 2, 3, 4);
    observable.subscribe(value => result.push(value));

    expect(result).toEqual([1, 2, 3, 4]);
  });

  test("creates an observable", () => {
    const result = [];

    const observable = from([1, 2, 3, 4]);
    observable.subscribe(value => {
      result.push(value);
    });

    expect(result).toEqual([1, 2, 3, 4]);
  });

  test("creates an observable from a promise that rejects", (done) => {
    const promise = Promise.reject({ error: "Something is wrong" });

    let error;

    const observable = from(promise);
    observable.subscribe({
      error: (error) => {
        expect(error).toEqual({ error: "Something is wrong" });
        done();
      }
    });
  });

  test("handle custom observable (BONUS)", () => {
    const result = [];

    const observable = new Observable(subscriber => {
      subscriber.next("Pepa");
      subscriber.next("Jozka");
      subscriber.next("Petra");
      subscriber.next("Aneta");
    });

    observable.subscribe(value => result.push(value));

    expect(result).toEqual(["Pepa", "Jozka", "Petra", "Aneta"]);
  });
});
