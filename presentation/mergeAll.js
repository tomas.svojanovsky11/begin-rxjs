const { map, delay, of, mergeAll } = require('rxjs');

const startingTime = Date.now();

const example = of(1, 2, 3, 4).pipe(
  map(value => of(value).pipe(
    delay(value * 1000),
  )),
  mergeAll(),
);

example.subscribe(value => {
  console.log({
    value,
    elapsedTime: Date.now() - startingTime,
  })
});
