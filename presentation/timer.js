const { timer } = require("rxjs");

const startingTime = Date.now();
const tick = timer(5000);

tick.subscribe(() => console.log(Date.now() - startingTime));
