const { from } = require("rxjs");
const { fibonacci } = require("../utilities/fibonacci.js");
const { mapTo, take, skipWhile } = require("rxjs/operators");

const over100 = from(fibonacci()).pipe(
  skipWhile(value => value < 100),
  take(4),
  mapTo("HELLO!!!"),
)

over100.subscribe(console.log);
