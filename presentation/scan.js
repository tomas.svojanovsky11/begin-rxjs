import { range } from "rxjs";
import { scan, take } from "rxjs/operators";

const fibonacci = range(0, Infinity).pipe(
  scan((total, value) => total + value, 0),
  take(4),
)

fibonacci.subscribe(console.log);
