const { of } = require("rxjs");

const example = of(1, 2, 3);

example.subscribe(value => console.log(value));
