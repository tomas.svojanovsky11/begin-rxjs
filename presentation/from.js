const { from } = require("rxjs");

const example = from(1, 2, 3);

example.subscribe(value => console.log(value));
