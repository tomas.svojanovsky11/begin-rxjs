const { of, map } = require("rxjs");

const double = of(1, 2, 3).pipe(map(n => n * 2));

double.subscribe(console.log);
