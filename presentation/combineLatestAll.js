import {of, map, interval, take, combineLatestAll } from "rxjs";

const example = of("a", "b", "c", "d").pipe(
  map(letter => {
    return interval(1000).pipe(
      map(i => `${letter} ${i}`),
      take(4)
    )
  }),
  combineLatestAll(),
);
