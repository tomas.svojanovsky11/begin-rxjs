const { of, filter } = require("rxjs");

const evenNumbers = of(1, 2, 3, 4, 5, 6, 7, 8).pipe(filter(n => n % 2 === 0));

evenNumbers.subscribe(console.log);
