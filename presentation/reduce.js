import { from } from "rxjs";
import { fibonacci } from "../utilities/fibonacci.js";
import { reduce, takeWhile } from "rxjs/operators";

const under200 = from(fibonacci()).pipe(
  takeWhile(value => value < 200),
  reduce((total, value) => total + value, 0),
);

under200.subscribe(console.log);
