const { interval, map, of, take, concatMap } = require("rxjs");

const example = of("a", "b", "c", "d")
  .pipe(concatMap(letter =>
      interval(1000).pipe(
        map(i => `${letter} ${i}`),
        take(4),
      )
    ),
  );

example.subscribe(console.log);
