const { of, map, delay } = require('rxjs');

const example = of(1, 2, 3, 4).pipe(
  map((value) => of(value).pipe(delay(value * 1000)))
)

example.subscribe(console.log);
