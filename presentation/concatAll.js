const { interval, map, of, take, concatAll } = require("rxjs");

const example = of("a", "b", "c", "d")
  .pipe(map(letter =>
      interval(1000).pipe(
        map(i => `${letter} ${i}`),
        take(4),
      )
    ),
    concatAll(),
  );

example.subscribe(console.log);
