import { fromEvent } from "rxjs";

const button = document.querySelector("button");

button.addEventListener("click", event => {
    console.log(event);
});

const buttonClick = fromEvent(button, "click");
buttonClick.subscribe(console.log);
