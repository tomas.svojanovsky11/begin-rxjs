import {
  debounceTime,
  distinctUntilChanged,
  fromEvent,
  map,
  mergeMap,
  switchMap,
  tap,
  of,
  merge,
  from,
  pluck,
} from 'rxjs';

import { fromFetch } from 'rxjs/fetch';

import {
  addResults,
  clearResults,
  endpointFor,
  search,
} from './utilities';

const endpoint = `http://localhost:3333/api/pokemon/search`;

// const search$ = fromEvent(search, 'input').pipe(
//   map(event => {
//     return event.target.value;
//   }),
//   mergeMap(searchTerm => { // kazdy keystroke vola api -> switchMap
//     return fromFetch(`${endpoint}/${searchTerm}`).pipe(
//       mergeMap(response => response.json()),
//     );
//   }),
//   tap(clearResults),
//   pluck('pokemon'),
//   tap(addResults),
// );
//
// const search$ = fromEvent(search, 'input').pipe(
//   map(event => {
//     return event.target.value;
//   }),
//   switchMap(searchTerm => {
//     // return fromFetch(`${endpoint}/${searchTerm}`).pipe(
//     return fromFetch(`${endpoint}/${searchTerm}?delay=1000&chaos=true`).pipe(
//       mergeMap(response => response.json()),
//     );
//   }),
//   tap(clearResults),
//   pluck('pokemon'),
//   tap(addResults),
// );


const search$ = fromEvent(search, 'input').pipe(
  debounceTime(300), // cekam az user dopise
  map(event => {
    return event.target.value;
  }),
  distinctUntilChanged(), // volam api jenom v pripade ze se zmeni input value
  switchMap(searchTerm => {
    return fromFetch(`${endpoint}/${searchTerm}?delay=1000&chaos=true`).pipe(
      mergeMap(response => response.json()),
    )
  }),
  tap(clearResults),
  pluck('pokemon'),
  tap(addResults),
);

search$.subscribe();
