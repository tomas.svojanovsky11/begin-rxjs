import {fromEvent, interval, merge, NEVER, scan } from 'rxjs';
import { skipUntil, takeUntil, mapTo, switchMap } from 'rxjs/operators';
import { setCount, startButton, pauseButton } from './utilities';

// interval, mapTo

// let interval$;

const start$ = fromEvent(startButton, 'click').pipe(mapTo(true));
const pause$ = fromEvent(pauseButton, 'click').pipe(mapTo(false));

// skipUntil
// takeUntil

const counter$ = merge(start$, pause$).pipe(
  switchMap((shouldBeRunning) => {
    if (shouldBeRunning) {
      return interval(1000);
    } else {
      return NEVER;
    }
  }),
  scan(value => value + 1, 0),
);

counter$.subscribe(setCount);

// start$.subscribe(() => {
//   interval$ = interval(1000).subscribe(setCount);
// });
//
// pause$.subscribe(() => {
//   interval$.unsubscribe();
// });

// pause$.subscribe(() => {
//   interval$.unsubscribe()
// });
//
// pause$.unsubscribe(() => {
//   subscription.unsubscribe();
// })
//
