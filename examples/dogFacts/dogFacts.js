import { fromEvent, mapTo, merge, NEVER, of, switchMap, timer } from "rxjs";
import { fromFetch} from "rxjs/fetch";
import { catchError, exhaustMap, mergeMap, tap, retry } from "rxjs/operators";

import { addFacts, clearError, clearFacts, fetchButton, setError, stopButton } from "./utilities";

const endpoint = "http://localhost:3333/api/facts?delay=200&chaos=true";

const fetchData = () => fromFetch(endpoint).pipe(
  mergeMap(response => {
    if (response.ok) {
      return response.json();
    } else {
      throw new Error('Something is wrong');
    }
  }),
  retry(3),
  catchError(error => {
    return of({ error: error.message });
  }),
)


const fetch$ = fromEvent(fetchButton, "click").pipe(mapTo(true))
const stop$ = fromEvent(stopButton, "click").pipe(mapTo(false))

const stream$ = merge(fetch$, stop$).pipe(
  switchMap(shouldFetch => {
    if (shouldFetch) {
      return timer(0, 5000)
        .pipe(
          tap(clearError),
          tap(clearFacts),
          exhaustMap(fetchData)
        );
    } else {
      return NEVER;
    }
  })
)

stream$.subscribe(addFacts);

// fetch.subscribe(({ facts, error }) => {
//   if (error) {
//     return setError(error);
//   }
//
//
//   addFacts({ facts })
// });
