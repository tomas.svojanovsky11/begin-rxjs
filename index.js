const { interval } = require("rxjs");

const startingTime = Date.now();
const tick = interval(1000);

tick.subscribe(() => console.log(Date.now() - startingTime));

